#!/bin/sh

# stops the execution of a script if a command or pipeline has an error
set -e

envsubst '$$APP_HOST $$APP_PORT $$LISTEN_PORT' \
    < /etc/nginx/default.conf.tpl > /etc/nginx/conf.d/default.conf

# directive tells Nginx to stay in the foreground
nginx -g 'daemon off;'
